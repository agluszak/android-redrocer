  Redrocer
============================

Języki i narzędzia programowania II 2019

Wprowadzenie
------------

Redrocer ("Recorder" od tyłu) jest aplikacją umożliwiającą nagrywanie plików dźwiękowych i odtwarzanie ich od tyłu. Pliki można zapisywać w pamięci urządzenia i udostępniać.

Zbudowany plik .apk -- http://students.mimuw.edu.pl/~ag385527/android/

Zastosowane technologie
-------------

* Kotlin
* Android Jetpack Architecture Components
   * Data Binding
   * NavigationUI
* Room Database (do przechowywania informacji o nagraniach)
* Constraint Layouts
* Recycler View
* FFMpeg (do odwracania nagrań)
* Timber (logowanie)

Przegląd architektury
-------------

Aplikacja została stworzona z wykorzystaniem .biblioteki kompatybilności wstecznej Jetpack. Jest jendo główne Activity i 3 fragmenty, do których przechodzi się za pomocą Navigation Drawer.

Na ekranie głównym są 4 przyciski. Jeden duży umożliwiający nagrywanie, które rozpoczyna się po dotknięciu i kończy po podniesieniu palca. Po nagraniu aktywują się dolne przyciski: odtwarzania, zapisania nagrania i odtwarzania od tyłu. Po zapisaniu aplikacja korzysta z kotlinowej korutyny do wrzucenia informacji do bazy i skopiowania nagrania z pliku tymczasowego do pliku głównego, a na koniec wyświetlenia Snackbara.

Fragment listy korzysta z Recycler View. Tak jak ekran główny umożliwia odtwarzanie, ale również udostępnianie plików na zewnątrz. Ekran ten zbudowany jest w oparciu o architekturę Model-View-ViewModel.

Ekran "O aplikacji" jest prostym statycznym tekstem z obrazkiem.

Dla zapewnienia obsługi udostępniania na nowszych urządzeniach aplikacja wykorzystuje Content Providera.

Po uruchomeniu aplikacja prosi o pozwolenie na nagrywanie dźwięku.


