package io.gitlab.agluszak.redrocer.screens.list

import android.app.Application
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.gitlab.agluszak.redrocer.R
import io.gitlab.agluszak.redrocer.databinding.FragmentListBinding
import io.gitlab.agluszak.redrocer.db.Recording
import io.gitlab.agluszak.redrocer.db.RecordingDatabase
import timber.log.Timber
import java.io.File
import java.io.IOException


class ListFragment : Fragment() {

    private var player: MediaPlayer? = null

    private lateinit var application: Application

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentListBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_list, container, false)

        application = requireNotNull(this.activity).application

        val dataSource = RecordingDatabase.getInstance(application).recordingDatabaseDao
        val viewModelFactory = ListViewModelFactory(dataSource, application)

        val listViewModel =
                ViewModelProviders.of(
                        this, viewModelFactory).get(ListViewModel::class.java)

        binding.listViewModel = listViewModel

        val adapter = RecordingAdapter(this)
        binding.recordingList.adapter = adapter


        listViewModel.recordings.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        binding.lifecycleOwner = this

        return binding.root
    }

    fun onShare(recording: Recording) {
        val sharePath = application.getExternalFilesDir(null)!!.absolutePath + "/" + recording.reversedFilename
        Timber.i(sharePath)

        val uri = FileProvider.getUriForFile(context!!, "io.gitlab.agluszak.fileprovider", File(sharePath))
        val share = Intent(Intent.ACTION_SEND)
        share.type = "audio/mp4"
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        share.putExtra(Intent.EXTRA_STREAM, uri)
        startActivity(Intent.createChooser(share, getString(R.string.share_title)))
    }

    private fun play(path: String) {
        stopPlaying()
        player = MediaPlayer().apply {
            try {
                setDataSource(path)
                prepare()

                setOnCompletionListener { stopPlaying() }
                start()
            } catch (e: IOException) {
                Timber.e("prepare() failed")
            }
        }
    }

    fun onPlay(recording: Recording) {
        if (player == null) {
            play(application.getExternalFilesDir(null)!!.absolutePath + "/" + recording.filename)
        } else {
            stopPlaying()
        }
    }

    fun onPlayReversed(recording: Recording) {
        if (player == null) {
            play(application.getExternalFilesDir(null)!!.absolutePath + "/" + recording.reversedFilename)
        } else {
            stopPlaying()
        }
    }

    private fun stopPlaying() {
        player?.stop()
        player?.release()
        player = null
    }
}