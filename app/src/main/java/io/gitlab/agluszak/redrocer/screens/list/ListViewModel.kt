package io.gitlab.agluszak.redrocer.screens.list

import android.app.Application
import androidx.lifecycle.ViewModel
import io.gitlab.agluszak.redrocer.db.RecordingDatabaseDao
import kotlinx.coroutines.Job


class ListViewModel(
        dataSource: RecordingDatabaseDao,
        application: Application) : ViewModel() {
    val database = dataSource
    private var viewModelJob = Job()

    val recordings = database.getAll()
}