package io.gitlab.agluszak.redrocer.screens.list


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.agluszak.redrocer.databinding.ListItemRecordingBinding
import io.gitlab.agluszak.redrocer.db.Recording

class RecordingAdapter(val fragment: ListFragment) : ListAdapter<Recording, RecordingAdapter.ViewHolder>(RecordingDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, fragment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemRecordingBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Recording, fragment: ListFragment) {
            binding.recording = item
            binding.fragment = fragment
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemRecordingBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}


class RecordingDiffCallback : DiffUtil.ItemCallback<Recording>() {

    override fun areItemsTheSame(oldItem: Recording, newItem: Recording): Boolean {
        return oldItem.recordingId == newItem.recordingId
    }

    override fun areContentsTheSame(oldItem: Recording, newItem: Recording): Boolean {
        return oldItem == newItem
    }
}
