package io.gitlab.agluszak.redrocer.db

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface RecordingDatabaseDao {

    @Insert
    fun insert(recording: Recording): Long

    @Update
    fun update(recording: Recording)

    @Delete
    fun delete(recording: Recording)

    @Query("SELECT * from recording_data_table WHERE recordingId = :key")
    fun get(key: Long): Recording

    @Query("DELETE FROM recording_data_table")
    fun clear()

    @Query("SELECT * FROM recording_data_table ORDER BY recordingId DESC")
    fun getAll(): LiveData<List<Recording>>

}
