package io.gitlab.agluszak.redrocer.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.DateFormat
import java.util.*

@Entity(tableName = "recording_data_table")
data class Recording(
        @PrimaryKey(autoGenerate = true)
        var recordingId: Long = 0L,

        @ColumnInfo(name = "title")
        val title: String,

        @ColumnInfo(name = "filename")
        val filename: String = "",

        @ColumnInfo(name = "reversed_filename")
        val reversedFilename: String = "",

        @ColumnInfo(name = "created_time_milli")
        val createdTimeMilli: Long = System.currentTimeMillis()
) {
    fun formattedTime(): String = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(Date(createdTimeMilli))
}