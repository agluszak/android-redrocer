package io.gitlab.agluszak.redrocer.screens.newrecording

import android.app.Application
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.arthenica.mobileffmpeg.FFmpeg
import com.google.android.material.snackbar.Snackbar
import io.gitlab.agluszak.redrocer.R
import io.gitlab.agluszak.redrocer.databinding.DialogNameBinding
import io.gitlab.agluszak.redrocer.databinding.FragmentNewRecordingBinding
import io.gitlab.agluszak.redrocer.db.Recording
import io.gitlab.agluszak.redrocer.db.RecordingDatabase
import io.gitlab.agluszak.redrocer.db.RecordingDatabaseDao
import io.gitlab.agluszak.redrocer.utils.Constants
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.File
import java.io.IOException


class NewRecordingFragment : Fragment() {

    private lateinit var fileName: String
    private lateinit var reversedFileName: String
    private lateinit var file: File
    private lateinit var fileReversed: File
    private lateinit var binding: FragmentNewRecordingBinding
    private lateinit var application: Application
    private lateinit var dataSource: RecordingDatabaseDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_new_recording, container, false)

        application = requireNotNull(this.activity).application

        dataSource = RecordingDatabase.getInstance(application).recordingDatabaseDao

        fileName = "audiorecordtest.mp4"
        reversedFileName = "rev.$fileName"
        file = File(application.getExternalFilesDir(null), fileName)
        if (file.exists()) {
            file.delete()
        }
        fileReversed = File(application.getExternalFilesDir(null), reversedFileName)
        if (fileReversed.exists()) {
            fileReversed.delete()
        }

        binding.recordButton.setOnTouchListener { v: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP ||
                    event.action == MotionEvent.ACTION_CANCEL) {
                binding.recordButton.text = getString(R.string.start_recording)
                stopRecording()
                true
            } else if (event.action == MotionEvent.ACTION_DOWN) {
                binding.recordButton.text = getString(R.string.done_recording)
                startRecording()
                true
            }
            false
        }

        binding.lifecycleOwner = this
        binding.recordingFragment = this

        return binding.root
    }

    private var recorder: MediaRecorder? = null
    private var player: MediaPlayer? = null
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun onPlay() {
        if (player == null) {
            play(file, binding.playButton)
        } else {
            stopPlaying()
        }
    }

    fun onPlayReversed() {
        if (player == null) {
            play(fileReversed, binding.playReverseButton)
        } else {
            stopPlaying()
        }
    }

    private suspend fun insert(recording: Recording) {
        withContext(Dispatchers.IO) {
            val id = dataSource.insert(recording)
            val filename = id.toString() + Constants.extension
            val reversedFilename = id.toString() + Constants.reversedTag + Constants.extension

            val updatedRecording = recording.copy(recordingId = id, filename = filename, reversedFilename = reversedFilename)
            dataSource.update(updatedRecording)

            val dir = application.getExternalFilesDir(null)!!.absolutePath
            val newFile = File(dir, filename)
            file.copyTo(newFile)
            val newFileReversed = File(dir, reversedFilename)
            fileReversed.copyTo(newFileReversed)
            Snackbar.make(binding.saveButton, R.string.saved, Snackbar.LENGTH_SHORT)
                    .show()
        }
    }

    private fun save(name: String) {
        uiScope.launch {
            val recording = Recording(title = name)
            insert(recording)
        }
    }

    fun onSave() {
        val builder = AlertDialog.Builder(activity!!, R.style.Theme_AppCompat_Light_Dialog)
        builder.setTitle(getString(R.string.name_prompt))
        val layoutInflater = LayoutInflater.from(context)
        val binding: DialogNameBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.dialog_name, null, false)
        val input = binding.input
        input.inputType = InputType.TYPE_CLASS_TEXT
        builder.setView(binding.root)
        builder.setPositiveButton(getString(R.string.ok)) { _, _ -> save(input.text.toString()) }
        builder.setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    private fun reverse() {
        val command = String.format("-y -i %s -af areverse %s", file.absolutePath, fileReversed.absoluteFile)
        FFmpeg.execute(command)
        Timber.i(FFmpeg.getLastCommandOutput())
    }

    private fun play(file: File, button: Button) {
        stopPlaying()
        player = MediaPlayer().apply {
            try {
                setDataSource(file.absolutePath)
                prepare()

                setOnCompletionListener { stopPlaying() }
                button.text = getString(R.string.stop)
                start()
            } catch (e: IOException) {
                Timber.e("prepare() failed")
            }
        }
    }

    private fun stopPlaying() {
        player?.stop()
        player?.release()
        player = null
        binding.playButton.text = getString(R.string.play)
        binding.playReverseButton.text = getString(R.string.play_reversed)
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(file.absolutePath)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            try {
                prepare()
            } catch (e: IOException) {
                Timber.e("prepare() failed")
            }

            start()
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            try {
                stop()
            } catch (e: RuntimeException) {
                Timber.e("stop() failed")
            }
            release()
            reverse()
            binding.playReverseButton.isEnabled = true
            binding.playButton.isEnabled = true
            binding.saveButton.isEnabled = true
        }
        recorder = null
    }

}