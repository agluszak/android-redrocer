package io.gitlab.agluszak.redrocer.utils

object Constants {
    const val extension = ".mp4"
    const val reversedTag = ".rev"
}